const React = require('react');
const ReactDOM = require('react-dom');
require('./style.scss')

const $ = require('jQuery');  
require('bootstrap-loader');

// React components
import Message from './message.js';

ReactDOM.render(<Message title="hello" message="sample message" />,
        document.getElementById('react-container'));