const React = require('react');

class Message extends React.Component {
    render() {
        return (<div>
                <h1>{this.props.title}</h1>
                <p>{this.props.message}</p>
            </div>);
    }
}
export default Message